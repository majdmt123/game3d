﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif





struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
struct RenderTextureU5BU5D_t9C963C4B9AAD862BBE402147E82F7BEBF699F6A6;
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
struct ArrayList_t7A8E5AF0C4378015B5731ABE2BED8F2782FEEF8A;
struct CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B;
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
struct Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692;
struct String_t;
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
struct U3CModuleU3E_t5C873E33121FECD351F7CB74331B5D342A4C978F 
{
};
struct U3CModuleU3E_t90149EF90407715CC46EB5A9704669888393F1DE 
{
};
struct U3CModuleU3E_tDA285F13E9413BF3B79A99D6E310BE9AF3444EEB 
{
};
struct U3CModuleU3E_t4791F64F4B6411D4D033A002CAD365D597AA2451 
{
};
struct U3CModuleU3E_tBF95EDE7AC72079FEC40B1A3B57D72418FF75C99 
{
};
struct U3CModuleU3E_t2C7BF608494A5C8FB8C8C4D318FB27BCF6CE322A 
{
};
struct U3CModuleU3E_t4BC86AB59C63F2CC6EB51BE560C15CFCAE821BC7 
{
};
struct U3CPrivateImplementationDetailsU3E_t5C1CD672F9513BAB468EFD53754405309697D1F6  : public RuntimeObject
{
};
struct U3CPrivateImplementationDetailsU3E_tDD68DE182EDDE48C043501A323E145FA4464421E  : public RuntimeObject
{
};
struct ASN1_t33549D58797C9C33AA83F13AD184EAA00C584A6F  : public RuntimeObject
{
	uint8_t ___m_nTag;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___m_aValue;
	ArrayList_t7A8E5AF0C4378015B5731ABE2BED8F2782FEEF8A* ___elist;
};
struct ASN1Convert_tDA6D2B7710D7868F3D559D5BE7F2C7816BB50AB6  : public RuntimeObject
{
};
struct Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA  : public RuntimeObject
{
};
struct BitConverterLE_tB6EF365ED05024FCC12DA3939B10FDEBDB29E1BD  : public RuntimeObject
{
};
struct ConfigurationElement_tAE3EE71C256825472831FFBB7F491275DFAF089E  : public RuntimeObject
{
};
struct ConfigurationPropertyCollection_t1DEB95D3283BB11A46B862E9D13710ED698B6C93  : public RuntimeObject
{
};
struct ConfigurationSectionGroup_tE7948C2D31B193F4BA8828947ED3094B952C7863  : public RuntimeObject
{
};
struct CryptoConvert_t676AC22DA6332E9936696ECC97197AB7B1BC7252  : public RuntimeObject
{
};
struct ShaderInput_t048F7464F457883E25851701A5BBF15ADDDABAA3  : public RuntimeObject
{
};
struct UnitySourceGeneratedAssemblyMonoScriptTypes_v1_tC03F5554B02AF8BD6DDC7FF1CE68CB8502B769E1  : public RuntimeObject
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};
struct XmlNode_t3180B9B3D5C36CD58F5327D9F13458E3B3F030AF  : public RuntimeObject
{
};
struct XmlReader_t4C709DEF5F01606ECB60B638F1BD6F6E0A9116FD  : public RuntimeObject
{
};
struct __Il2CppComDelegate_tD0DD2BBA6AC8F151D32B6DFD02F6BDA339F8DC4D  : public Il2CppComObject
{
};
struct ConfigurationCollectionAttribute_t1D7DBAAB4908B6B8F26EA1C66106A67BDE949558  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
};
struct ConfigurationElementCollection_t56E8398661A85A59616301BADF13026FB1492606  : public ConfigurationElement_tAE3EE71C256825472831FFBB7F491275DFAF089E
{
};
struct ConfigurationSection_t0BC609F0151B160A4FAB8226679B62AF22539C3E  : public ConfigurationElement_tAE3EE71C256825472831FFBB7F491275DFAF089E
{
};
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	int32_t ___m_value;
};
struct IntPtr_t 
{
	void* ___m_value;
};
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	float ___x;
	float ___y;
	float ___z;
};
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	float ___x;
	float ___y;
	float ___z;
	float ___w;
};
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D275_tC45C046D9EF24647ED9CF1F8EB3971C6EA3666E3 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D275_tC45C046D9EF24647ED9CF1F8EB3971C6EA3666E3__padding[275];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D89_tFDA6E3921D3798638D01F2A238020CF39BA27C0D 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D89_tFDA6E3921D3798638D01F2A238020CF39BA27C0D__padding[89];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D15_t350D5D43360F35D86B7F368AEA1A2064A6BDE0B3 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D15_t350D5D43360F35D86B7F368AEA1A2064A6BDE0B3__padding[15];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D93_tF45FCCD8C0E677F664F254F53982ACB8F99D0852 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D93_tF45FCCD8C0E677F664F254F53982ACB8F99D0852__padding[93];
	};
};
#pragma pack(pop, tp)
struct MonoScriptData_t3C947E566E2B4D528827132CDF6C195EC7DE6F91 
{
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___FilePathsData;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___TypesData;
	int32_t ___TotalTypes;
	int32_t ___TotalFiles;
	bool ___IsEditorOnly;
};
struct MonoScriptData_t3C947E566E2B4D528827132CDF6C195EC7DE6F91_marshaled_pinvoke
{
	Il2CppSafeArray* ___FilePathsData;
	Il2CppSafeArray* ___TypesData;
	int32_t ___TotalTypes;
	int32_t ___TotalFiles;
	int32_t ___IsEditorOnly;
};
struct MonoScriptData_t3C947E566E2B4D528827132CDF6C195EC7DE6F91_marshaled_com
{
	Il2CppSafeArray* ___FilePathsData;
	Il2CppSafeArray* ___TypesData;
	int32_t ___TotalTypes;
	int32_t ___TotalFiles;
	int32_t ___IsEditorOnly;
};
struct Exception_t  : public RuntimeObject
{
	String_t* ____className;
	String_t* ____message;
	RuntimeObject* ____data;
	Exception_t* ____innerException;
	String_t* ____helpURL;
	RuntimeObject* ____stackTrace;
	String_t* ____stackTraceString;
	String_t* ____remoteStackTraceString;
	int32_t ____remoteStackIndex;
	RuntimeObject* ____dynamicMethods;
	int32_t ____HResult;
	String_t* ____source;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces;
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips;
	int32_t ___caught_in_unmanaged;
};
struct Exception_t_marshaled_pinvoke
{
	char* ____className;
	char* ____message;
	RuntimeObject* ____data;
	Exception_t_marshaled_pinvoke* ____innerException;
	char* ____helpURL;
	Il2CppIUnknown* ____stackTrace;
	char* ____stackTraceString;
	char* ____remoteStackTraceString;
	int32_t ____remoteStackIndex;
	Il2CppIUnknown* ____dynamicMethods;
	int32_t ____HResult;
	char* ____source;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces;
	Il2CppSafeArray* ___native_trace_ips;
	int32_t ___caught_in_unmanaged;
};
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className;
	Il2CppChar* ____message;
	RuntimeObject* ____data;
	Exception_t_marshaled_com* ____innerException;
	Il2CppChar* ____helpURL;
	Il2CppIUnknown* ____stackTrace;
	Il2CppChar* ____stackTraceString;
	Il2CppChar* ____remoteStackTraceString;
	int32_t ____remoteStackIndex;
	Il2CppIUnknown* ____dynamicMethods;
	int32_t ____HResult;
	Il2CppChar* ____source;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces;
	Il2CppSafeArray* ___native_trace_ips;
	int32_t ___caught_in_unmanaged;
};
struct IgnoreSection_t43A7C33C0083D18639AA3CC3D75DD93FCF1C5D97  : public ConfigurationSection_t0BC609F0151B160A4FAB8226679B62AF22539C3E
{
};
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	intptr_t ___m_CachedPtr;
};
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr;
};
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr;
};
struct LightData_tAC4023737E9903DE3F96B993AA323E062ABCB9ED 
{
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___position;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___color;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___attenuation;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___spotDirection;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___occlusionProbeChannels;
	uint32_t ___layerMask;
};
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};
struct InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};
struct Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};
struct GridLayout_tAD661B1E1E57C16BE21C8C13432EA04FE1F0418B  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ___m_CancellationTokenSource;
};
struct ObjectDisposedException_tC5FB29E8E980E2010A2F6A5B9B791089419F89EB  : public InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB
{
	String_t* ____objectName;
};
struct SpriteShapeRenderer_tE998BB73CF661079736CCC23617E597AB230A4AC  : public Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF
{
};
struct Bloom_tA14A4ADA32420926093CB5242D06D17DA1A9BB1E  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	float ____threshold;
	float ____softKnee;
	float ____radius;
	float ____intensity;
	bool ____highQuality;
	bool ____antiFlicker;
	Shader_tADC867D36B7876EE22427FAA2CE485105F4EE692* ____shader;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ____material;
	RenderTextureU5BU5D_t9C963C4B9AAD862BBE402147E82F7BEBF699F6A6* ____blurBuffer1;
	RenderTextureU5BU5D_t9C963C4B9AAD862BBE402147E82F7BEBF699F6A6* ____blurBuffer2;
};
struct CFXR_Demo_Rotate_tC9BF71B92F879B0DC058EC49386E2B596A3021F2  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___axis;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___center;
	float ___speed;
};
struct CFXR_Demo_Translate_t95CB6CDBAE93F397B9CA608E9B233BAB8C7FB891  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___direction;
	bool ___randomRotation;
	bool ___initialized;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___initialPosition;
};
struct Grid_t4C0ACF986206D18240E35155E01B5A97DAA651FE  : public GridLayout_tAD661B1E1E57C16BE21C8C13432EA04FE1F0418B
{
};
struct ThrowStub_t9161280E38728A40D9B1A975AEE62E89C379E400  : public ObjectDisposedException_tC5FB29E8E980E2010A2F6A5B9B791089419F89EB
{
};
struct U3CPrivateImplementationDetailsU3E_t5C1CD672F9513BAB468EFD53754405309697D1F6_StaticFields
{
	__StaticArrayInitTypeSizeU3D275_tC45C046D9EF24647ED9CF1F8EB3971C6EA3666E3 ___AEE6A7B42B752A422B047101344F674E1151195D596B559FF2982A8986C43918;
	__StaticArrayInitTypeSizeU3D89_tFDA6E3921D3798638D01F2A238020CF39BA27C0D ___F671D3138F4BC713270016A7DAD7FF9EB839ABDC647E4A6BFC97C8F96C3C3CF8;
};
struct U3CPrivateImplementationDetailsU3E_tDD68DE182EDDE48C043501A323E145FA4464421E_StaticFields
{
	__StaticArrayInitTypeSizeU3D15_t350D5D43360F35D86B7F368AEA1A2064A6BDE0B3 ___7E87B7B8B7CD3CEB2DDE5E25E4AE6A76F26A8C3B427BC7F597BB408E1687C904;
	__StaticArrayInitTypeSizeU3D93_tF45FCCD8C0E677F664F254F53982ACB8F99D0852 ___AAE3D9D212B5DF82AFB973B49A2E8F679D43D71D80D3C10B86EB01FDFAC37EF3;
};
struct XmlReader_t4C709DEF5F01606ECB60B638F1BD6F6E0A9116FD_StaticFields
{
	uint32_t ___IsTextualNodeBitmap;
	uint32_t ___CanReadContentAsBitmap;
	uint32_t ___HasValueBitmap;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5400;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5400 = { sizeof(CFXR_Demo_Rotate_tC9BF71B92F879B0DC058EC49386E2B596A3021F2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5401;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5401 = { sizeof(CFXR_Demo_Translate_t95CB6CDBAE93F397B9CA608E9B233BAB8C7FB891), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5402;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5402 = { sizeof(__StaticArrayInitTypeSizeU3D89_tFDA6E3921D3798638D01F2A238020CF39BA27C0D)+ sizeof(RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D89_tFDA6E3921D3798638D01F2A238020CF39BA27C0D), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5403;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5403 = { sizeof(__StaticArrayInitTypeSizeU3D275_tC45C046D9EF24647ED9CF1F8EB3971C6EA3666E3)+ sizeof(RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D275_tC45C046D9EF24647ED9CF1F8EB3971C6EA3666E3), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5404;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5404 = { sizeof(U3CPrivateImplementationDetailsU3E_t5C1CD672F9513BAB468EFD53754405309697D1F6), -1, sizeof(U3CPrivateImplementationDetailsU3E_t5C1CD672F9513BAB468EFD53754405309697D1F6_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5405;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5405 = { sizeof(U3CModuleU3E_t2C7BF608494A5C8FB8C8C4D318FB27BCF6CE322A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5406;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5406 = { sizeof(Grid_t4C0ACF986206D18240E35155E01B5A97DAA651FE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5407;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5407 = { sizeof(GridLayout_tAD661B1E1E57C16BE21C8C13432EA04FE1F0418B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5408;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5408 = { sizeof(U3CModuleU3E_t4BC86AB59C63F2CC6EB51BE560C15CFCAE821BC7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5409;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5409 = { sizeof(SpriteShapeRenderer_tE998BB73CF661079736CCC23617E597AB230A4AC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5410;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5410 = { sizeof(U3CModuleU3E_t90149EF90407715CC46EB5A9704669888393F1DE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5411;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5411 = { sizeof(ASN1_t33549D58797C9C33AA83F13AD184EAA00C584A6F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5412;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5412 = { sizeof(ASN1Convert_tDA6D2B7710D7868F3D559D5BE7F2C7816BB50AB6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5413;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5413 = { sizeof(BitConverterLE_tB6EF365ED05024FCC12DA3939B10FDEBDB29E1BD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5414;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5414 = { sizeof(CryptoConvert_t676AC22DA6332E9936696ECC97197AB7B1BC7252), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5415;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5415 = { sizeof(U3CModuleU3E_t5C873E33121FECD351F7CB74331B5D342A4C978F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5416;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5416 = { sizeof(MonoScriptData_t3C947E566E2B4D528827132CDF6C195EC7DE6F91)+ sizeof(RuntimeObject), sizeof(MonoScriptData_t3C947E566E2B4D528827132CDF6C195EC7DE6F91_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5417;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5417 = { sizeof(UnitySourceGeneratedAssemblyMonoScriptTypes_v1_tC03F5554B02AF8BD6DDC7FF1CE68CB8502B769E1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5418;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5418 = { sizeof(Bloom_tA14A4ADA32420926093CB5242D06D17DA1A9BB1E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5419;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5419 = { sizeof(__StaticArrayInitTypeSizeU3D15_t350D5D43360F35D86B7F368AEA1A2064A6BDE0B3)+ sizeof(RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D15_t350D5D43360F35D86B7F368AEA1A2064A6BDE0B3), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5420;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5420 = { sizeof(__StaticArrayInitTypeSizeU3D93_tF45FCCD8C0E677F664F254F53982ACB8F99D0852)+ sizeof(RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D93_tF45FCCD8C0E677F664F254F53982ACB8F99D0852), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5421;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5421 = { sizeof(U3CPrivateImplementationDetailsU3E_tDD68DE182EDDE48C043501A323E145FA4464421E), -1, sizeof(U3CPrivateImplementationDetailsU3E_tDD68DE182EDDE48C043501A323E145FA4464421E_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5422;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5422 = { sizeof(U3CModuleU3E_t4791F64F4B6411D4D033A002CAD365D597AA2451), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5423;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5423 = { sizeof(XmlReader_t4C709DEF5F01606ECB60B638F1BD6F6E0A9116FD), -1, sizeof(XmlReader_t4C709DEF5F01606ECB60B638F1BD6F6E0A9116FD_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5424;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5424 = { sizeof(XmlNode_t3180B9B3D5C36CD58F5327D9F13458E3B3F030AF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5425;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5425 = { sizeof(U3CModuleU3E_tBF95EDE7AC72079FEC40B1A3B57D72418FF75C99), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5426;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5426 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5427;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5427 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5428;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5428 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5429;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5429 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5430;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5430 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5431;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5431 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5432;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5432 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5433;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5433 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5434;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5434 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5435;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5435 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5436;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5436 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5437;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5437 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5438;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5438 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5439;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5439 = { sizeof(LightData_tAC4023737E9903DE3F96B993AA323E062ABCB9ED)+ sizeof(RuntimeObject), sizeof(LightData_tAC4023737E9903DE3F96B993AA323E062ABCB9ED), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5440;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5440 = { sizeof(ShaderInput_t048F7464F457883E25851701A5BBF15ADDDABAA3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5441;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5441 = { sizeof(U3CModuleU3E_tDA285F13E9413BF3B79A99D6E310BE9AF3444EEB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5442;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5442 = { sizeof(ConfigurationElement_tAE3EE71C256825472831FFBB7F491275DFAF089E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5443;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5443 = { sizeof(ConfigurationSection_t0BC609F0151B160A4FAB8226679B62AF22539C3E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5444;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5444 = { sizeof(int32_t)+ sizeof(RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5445;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5445 = { sizeof(ConfigurationPropertyCollection_t1DEB95D3283BB11A46B862E9D13710ED698B6C93), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5446;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5446 = { sizeof(ConfigurationElementCollection_t56E8398661A85A59616301BADF13026FB1492606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5447;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5447 = { sizeof(ConfigurationCollectionAttribute_t1D7DBAAB4908B6B8F26EA1C66106A67BDE949558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5448;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5448 = { sizeof(ConfigurationSectionGroup_tE7948C2D31B193F4BA8828947ED3094B952C7863), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5449;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5449 = { sizeof(IgnoreSection_t43A7C33C0083D18639AA3CC3D75DD93FCF1C5D97), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5450;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5450 = { sizeof(ThrowStub_t9161280E38728A40D9B1A975AEE62E89C379E400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5451;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5451 = { 0, sizeof(Il2CppIActivationFactory*), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5452;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5452 = { sizeof(Il2CppComObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5453;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5453 = { sizeof(__Il2CppComDelegate_tD0DD2BBA6AC8F151D32B6DFD02F6BDA339F8DC4D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5454;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5454 = { sizeof(Il2CppFullySharedGenericAny), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5455;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize5455 = { sizeof(Il2CppFullySharedGenericStruct)+ sizeof(RuntimeObject), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
