using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ZombieModel
{
    public GameObject zombiePrefab;
    public float zombieHealth;
    public float range;
    public float damage;
    public float speed;
    public float speedAttack;
    public float timeSummon;
    public int cost;
    public int amount;

    public GameObject GetZombiePrefab()
    {
        return zombiePrefab;
    }

    public int GetCost()
    {
        return cost;
    }

    public float GetHealth()
    {
        return zombieHealth;
    }

    public float GetRange()
    {
        return range;
    }
    public float GetDamage()
    {
        return damage;
    }
    public float GetSpeed()
    {
        return speed;
    }

    public int GetAmount()
    {
        return amount;
    }
}
