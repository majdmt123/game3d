using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BulletModel
{
    public GameObject prefab;
    public float damage;
    public float speed;

    public GameObject GetPrefab() 
    { 
        return prefab; 
    }

    public float GetDamage()
    {
        return damage;
    }

    public float GetSpeed()
    {
        return speed;
    }
}
