using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HeroModel
{
    public GameObject heroPrefab;
    public float heroHealth;
    public float range;
    public float damage;
    public float speedAttack;
    public int cost;
    public BulletModel bulletModel;

    public GameObject GetHeroPrefab()
    {
        return heroPrefab;
    }

    public int GetCost()
    {
        return cost;
    }

    public float GetHealth()
    {
        return heroHealth;
    }

    public float GetRange()
    {
        return range;
    }
    public float GetDamage()
    {
        return damage;
    }

    public BulletModel GetBulletModel()
    {
        return bulletModel;
    }
}
