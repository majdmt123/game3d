using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombHeroController : HeroController
{
    public override void Start()
    {
        base.Start();
        animator.SetBool("isExplosioning", true);
    }

    public override void Update()
    {

    }
    public void Explode()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, heroModel.GetRange());
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "Zombie")
            {
                collider.gameObject.GetComponent<ZombieController>().Hit(heroModel.GetDamage(), true);
            }
        }
        HeroTakeDamage(100);
        audioManager.ExplosionSound();
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, heroModel.GetRange());
    }
}
