using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldHeroController : HeroController
{
    private float heroShield;

    public override void FindEnemy(bool isTargetSeen)
    {
        animator.SetBool("isDefensing", isTargetSeen);
    }


}
