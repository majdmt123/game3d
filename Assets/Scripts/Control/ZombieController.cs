using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieController : MonoBehaviour
{
    private ZombieModel zombieModel;

    private ZombieView zombieView;

    private LevelController levelController;

    private Shop shop;

    public LayerMask shootMark;

    private Animator animator;

    private GameManager gameManager;

    private AudioManager audioManager;

    private float zombieMaxHealth;
    private float zombieCurrentHealth;

    private float zombieCurrentSpeed;
    private void Start()
    {
        levelController = GameObject.Find("Level").GetComponent<LevelController>();
        zombieModel = levelController.GetZombieModel();
        animator = GetComponent<Animator>();
        shop = GameObject.Find("Shop").GetComponent<Shop>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        zombieView = GetComponent<ZombieView>();

        zombieMaxHealth = zombieModel.GetHealth();
        zombieCurrentHealth = zombieModel.GetHealth();
        zombieCurrentSpeed = zombieModel.GetSpeed();

    }
    private void Update()
    {
        if (transform.position.x < -4.3)
        {
            Destroy(gameObject);
            gameManager.LoseGame();
        }

        RaycastHit hit;
        bool isTargetSeen = Physics.Raycast(transform.position, transform.forward, out hit, zombieModel.GetRange(), shootMark);
        FindHero(isTargetSeen);

        if (!isTargetSeen)
        {
            transform.position -= new Vector3(zombieCurrentSpeed, 0, 0) * Time.deltaTime;
        }

        zombieCurrentSpeed = zombieModel.GetSpeed();


    }

    public void FindHero(bool isTargetSeen)
    {
        animator.SetBool("isAttacking", isTargetSeen);
    }

    public void Hit(float damage, bool isHadEffect)
    {
        if (isHadEffect)
        {
            Instantiate(zombieView.takeDamageEffect, zombieView.takeDamageEffectPos.transform.position, Quaternion.identity);
        }
        zombieCurrentHealth -= damage;
        zombieView.UpdateHealthBar(zombieMaxHealth, zombieCurrentHealth);
        if (zombieCurrentHealth <= 0)
        {
            shop.KilledZombie(zombieModel);
            Destroy(gameObject);
            levelController.ZombieDied();

        }
    }

    public void ZombieAttack()
    {
        RaycastHit hit;
        bool isTargetSeen = Physics.Raycast(transform.position, transform.forward, out hit, zombieModel.GetRange(), shootMark);
        if (isTargetSeen)
        {
            audioManager.ZombieHitSound();
            hit.collider.GetComponent<HeroController>().HeroTakeDamage(zombieModel.GetDamage());
        }
    }

    public void ZombieSlowed(float slowPct)
    {
        zombieCurrentSpeed = zombieModel.GetSpeed() * (1 - slowPct);
    }
}
