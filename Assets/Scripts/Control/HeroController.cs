using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroController : MonoBehaviour
{
    [HideInInspector]
    //Tham chieu den HeroModel
    public HeroModel heroModel;

    //Tham chieu den BuildingSystem
    private BuildingSystem buildingSystem;

    [HideInInspector]
    //Tham chieu den HeroView
    public HeroView heroView;

    [HideInInspector]
    //Tham chieu den Animator
    public Animator animator;

    [HideInInspector]
    //Tham chieu den AudioManager
    public AudioManager audioManager;

    //Max health cua Hero
    private float heroMaxHealth;

    //Mau hien tai cua Hero
    private float heroCurrentHealth;

    //Cheking to Zombie
    public RaycastHit hit;

    [HideInInspector]
    public bool isTargetSeen;

    public virtual void Start()
    {
        //GetComponent BuildingSystem
        buildingSystem = GameObject.Find("BuildingSystem").GetComponent<BuildingSystem>();

        //GetComponent AudioManager
        audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();

        //GetComponent HeroView
        heroView = GetComponent<HeroView>();

        //GetComponent Animator
        animator = GetComponent<Animator>();

        //Khoi tao HeroModel
        heroModel = new HeroModel();
        
        //Gan gia tri cua doi tuong heroModel bang doi tuong HeroModel vua Build 
        heroModel = buildingSystem.GetHeroModel1();

        //Gan gia tri heroMaxHealth va heroCurrentHealth bang voi thuoc tinh cua heroModel
        heroMaxHealth = heroModel.GetHealth();
        heroCurrentHealth = heroModel.GetHealth();
        
    }

    public virtual void Update()
    {
        isTargetSeen = Physics.Raycast(transform.position, transform.forward, out hit, heroModel.GetRange(), heroView.shootMark);
        FindEnemy(isTargetSeen);
    }

    //Chuyen doi Node ve mac dinh
    public void TakeNode()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -transform.up, out hit))
        {
            GameObject hitObject = hit.collider.gameObject;
            hitObject.GetComponent<Node>().ResetNode();
        }
    }

    //Hero nhan sat thuong
    public void HeroTakeDamage(float damage)
    {
        Instantiate(heroView.takeDamageEffect, heroView.takeDamageEffectPos.transform.position, Quaternion.identity);
        heroCurrentHealth -= damage;
        heroView.UpdateHealthBar(heroMaxHealth, heroCurrentHealth);
        if (heroCurrentHealth <= 0)
        {
            TakeNode();
            Destroy(gameObject);
        }
    }

    //Chuyen sang animation Attacking
    public virtual void FindEnemy(bool isTargetSeen)
    {
        animator.SetBool("isAttacking", isTargetSeen);
    }
    
    public void HeroAttack()
    {
        audioManager.SwordHitSound();
        hit.collider.GetComponent<ZombieController>().Hit(heroModel.GetDamage(), true);
    }
}
