using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunnerHeroController : HeroController
{
    public int poolSize = 5;
    public Queue<GameObject> bulletPool = new Queue<GameObject>();
    public override void Start()
    {
        base.Start();
        for (int i = 0; i < poolSize; i++)
        {
            GameObject bulletIns = Instantiate(heroModel.GetBulletModel().GetPrefab(), heroView.shootOrigin.position, Quaternion.identity);
            bulletIns.SetActive(false);
            bulletPool.Enqueue(bulletIns);
        }
    }

    public override void FindEnemy(bool isTargetSeen)
    {
        animator.SetBool("isRBShoot", isTargetSeen);
    }

    public void GetBulletFromPool()
    {
        audioManager.ShotSound();
        if (bulletPool.Count > 0)
        {
            GameObject bulletIns = bulletPool.Dequeue();
            bulletIns.SetActive(true);
            bulletIns.transform.position = heroView.shootOrigin.position;
        }
        else
        {

            Instantiate(heroModel.GetBulletModel().GetPrefab(), heroView.shootOrigin.position, Quaternion.identity);
        }

    }
    public void ReturnBulletToPool(GameObject bulletIns )
    {
        bulletPool.Enqueue(bulletIns);
        bulletIns.SetActive(false);
    }
}
