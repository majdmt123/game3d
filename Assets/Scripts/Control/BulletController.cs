using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    private BulletModel bulletModel;

    private GunnerHeroController gunnerHeroController;
    // Start is called before the first frame update
    void Start()
    {
        RaycastHit hit;
        Physics.Raycast(transform.position, -transform.right, out hit);
        if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Hero"))
        {
            gunnerHeroController = hit.collider.gameObject.GetComponent<GunnerHeroController>();
        }

        bulletModel = gunnerHeroController.heroModel.GetBulletModel();
    }

    // Update is called once per frame
    void Update()
    {

        Shoot();
        BulletOutDisplay();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<ZombieController>(out ZombieController zombieController))
        {

            zombieController.Hit(bulletModel.GetDamage(), true);

            gunnerHeroController.ReturnBulletToPool(gameObject);
            //Instantiate(bulletEffect, gameObject.transform.position, Quaternion.identity);
        }
    }

    private void Shoot()
    {
        transform.position = transform.position + new Vector3(bulletModel.GetSpeed() * Time.deltaTime, 0, 0);
    }

    private void BulletOutDisplay()
    {
        if (transform.position.x >= 10f)
            gunnerHeroController.ReturnBulletToPool(gameObject);
    }
}
