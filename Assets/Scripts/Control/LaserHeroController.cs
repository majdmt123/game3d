using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserHeroController : HeroController
{
    public float slowPct = .5f;

    public override void FindEnemy(bool isTargetSeen)
    {
        animator.SetBool("isShooting", isTargetSeen);
    }

    public override void Update()
    {
        base.Update();
        LaserAttack();
    }
    public void LaserAttack()
    {
        if (isTargetSeen)
        {
            Vector3 zombieSize = hit.collider.bounds.size;
            if (!heroView.lineRenderer.enabled)
            {
                heroView.lineRenderer.enabled = true;
                heroView.impactEffect.Play();
                heroView.impactLight.enabled = true;
            }
            
            heroView.lineRenderer.SetPosition(0, heroView.shootOrigin.position);
            heroView.lineRenderer.SetPosition(1, hit.collider.gameObject.transform.position + new Vector3(0, heroView.shootOrigin.position.y - 0.2f, 0));
            heroView.impactEffect.transform.position = hit.collider.gameObject.transform.position + new Vector3(0, heroView.shootOrigin.position.y - 0.2f, 0);
            hit.collider.GetComponent<ZombieController>().Hit(heroModel.GetDamage() * Time.deltaTime, false);
            hit.collider.GetComponent<ZombieController>().ZombieSlowed(slowPct);

            //audioManager.LaserSound();
            //audioManager.isPlay = false;
        }
        else
        {
            heroView.lineRenderer.enabled = false;
            heroView.impactEffect.Stop();
            heroView.impactLight.enabled = false;

            //audioManager.OffLaserSound();
            //audioManager.isPlay = true;
        }

        
    }


}
