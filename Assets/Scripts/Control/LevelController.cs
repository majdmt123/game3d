using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    [SerializeField]
    private Level level;

    private LevelView levelView;

    public float timeBetweenWaves = 5f;

    private float countdown = 2f;

    private int waveIndex = 0;

    private int EnemiesAlivesWave = 0;

    private int zombieAmounts = 0;

    public GameObject spawnZombieEffect;

    public GameManager gameManager;

    private ZombieModel zombieModel;

    private void Start()
    {
        levelView = GetComponent<LevelView>();

        for (int i = 0; i < level.waves.Length; i++)
        {
            for (int j = 0; j < level.waves[i].zombieModels.Length; j++)
            {
                zombieAmounts += level.waves[i].zombieModels[j].amount;
            }
        }

        levelView.totalWave.text = level.waves.Length.ToString();

        levelView.levelNameUI.text = SceneManager.GetActiveScene().name;
    }

    private void Update()
    {
        if (zombieAmounts == 0)
        {
            gameManager.WinGame();
            this.enabled = false;
        }

        if (EnemiesAlivesWave > 0)
        {
            return;
        }

        if (countdown <= 0)
        {
            StartCoroutine(SpawnZombie());
            countdown = timeBetweenWaves;
            return;
        }

        if (waveIndex < level.waves.Length)
        {
            levelView.currentWave.text = (waveIndex + 1).ToString();

            countdown -= Time.deltaTime;

            countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);

            levelView.waveCountdownText.text = string.Format("{0:00.00}", countdown);
        }

    }

    public void ZombieDied()
    {
        zombieAmounts--;
        EnemiesAlivesWave--;
    }

    IEnumerator SpawnZombie()
    {
        Wave wave = level.waves[waveIndex];

        for (int i = 0; i < wave.zombieModels.Length; i++)
        {
            EnemiesAlivesWave += wave.zombieModels[i].amount;
        }
        StartCoroutine(SpawnZombies(waveIndex, 0, 0));
        StartCoroutine(SpawnZombies(waveIndex, 1, 5));
        StartCoroutine(SpawnZombies(waveIndex, 2, 7));
        waveIndex++;

        yield return null;
    }

    IEnumerator SpawnZombies(int waveIndex, int zombieModelsIndex, int timeSummonFirstZombie)
    {
        yield return new WaitForSeconds(timeSummonFirstZombie);
        for (int i = 0; i < level.waves[waveIndex].zombieModels[zombieModelsIndex].amount; i++)
        {
            int r = Random.Range(0, level.spawnPoints.Length);
            Instantiate(spawnZombieEffect, level.spawnPoints[r].position, Quaternion.Euler(-90, 0, 0));
            GameObject myZombie = Instantiate(level.waves[waveIndex].zombieModels[zombieModelsIndex].GetZombiePrefab(), level.spawnPoints[r].position, Quaternion.Euler(0, -90, 0));
            zombieModel = level.waves[waveIndex].zombieModels[zombieModelsIndex];
            yield return new WaitForSeconds(level.waves[waveIndex].zombieModels[zombieModelsIndex].timeSummon);
        }
    }

    public ZombieModel GetZombieModel()
    {
        return zombieModel;
    }
}
