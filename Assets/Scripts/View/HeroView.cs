using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroView : MonoBehaviour
{
    //HealthBarUI
    public Image healthBar;

    //LayerMask to Hero attack
    public LayerMask shootMark;

    public GameObject takeDamageEffect;

    public GameObject takeDamageEffectPos;

    //Position bullet shoot
    public Transform shootOrigin;

    public LineRenderer lineRenderer;

    public ParticleSystem impactEffect;

    public Light impactLight;
    //UpdateHealthBar
    public void UpdateHealthBar(float maxHealth, float currentHealth)
    {
        healthBar.fillAmount = currentHealth / maxHealth;
    }
}
