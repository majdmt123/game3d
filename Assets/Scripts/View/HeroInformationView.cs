using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HeroInformationView : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject heroInfoUI;
    public void OnPointerEnter(PointerEventData eventData)
    {
        if(!heroInfoUI.activeSelf) {
            heroInfoUI.SetActive(true);
        }
        
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        heroInfoUI.SetActive(false);
    }

}
