using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZombieView : MonoBehaviour
{
    public Image zombieHealthBar;

    public GameObject takeDamageEffect;

    public GameObject takeDamageEffectPos;
    public void UpdateHealthBar(float maxHealth, float currentHealth)
    {
        zombieHealthBar.fillAmount = currentHealth / maxHealth;
    }
}
