using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LevelView : MonoBehaviour
{
    public TextMeshProUGUI waveCountdownText;
    public TextMeshProUGUI currentWave;
    public TextMeshProUGUI totalWave;
    public TextMeshProUGUI levelNameUI;
}
