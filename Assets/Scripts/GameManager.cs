using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject loseGameGUI;
    public GameObject winGameGUI;
    public GameObject pauseGameGUI;

    public int levelToUnlock;

    private int levelMaxUnlocked;

    public SceneFader sceneFader;
    public AudioManager audioManager;

    public GameObject buildingSystem;
    private void Start()
    {
        levelMaxUnlocked = PlayerPrefs.GetInt("levelReached");
        //Debug.Log(levelMaxUnlocked);
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            PauseGame();
        }
    }
    public void LoseGame()
    {
        audioManager.LoseSound();
        Time.timeScale = 0f;
        if (!loseGameGUI.activeSelf)
            loseGameGUI.SetActive(true);
        
    }

    public void WinGame()
    {
        audioManager.WinSound();
        if (!winGameGUI.activeSelf) 
            winGameGUI.SetActive(true);
        if(levelMaxUnlocked < levelToUnlock)
        {
            levelMaxUnlocked = levelToUnlock;
        }
        PlayerPrefs.SetInt("levelReached", levelMaxUnlocked);
    }

    public void Restart()
    {
        
        //sceneFader.FadeTo(SceneManager.GetActiveScene().name);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Time.timeScale = 1f;
    }

    public void PauseGame()
    {
        if (!pauseGameGUI.activeSelf)
        {
            pauseGameGUI.SetActive(true);
            buildingSystem.SetActive(false);
            Time.timeScale = 0f;

            //audioManager.OffLaserSound();
        }
        else
        {
            pauseGameGUI.SetActive(false);
            buildingSystem.SetActive(true);
            Time.timeScale = 1f;

            //audioManager.isPlay = true;
            //audioManager.LaserSound();
        }
    }
    public void Menu()
    {
        SceneManager.LoadScene("MenuScene");
        //sceneFader.FadeTo("MenuScene");
        Time.timeScale = 1f;
        
    }

    public void NextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
