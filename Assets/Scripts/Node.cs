using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    private Renderer nodeRenderer;
    private Color originColor;
    public bool hasHero = false;

    public BuildingSystem buildingSystem;

    private void Start()
    {
        //buildingSystem = GameObject.Find("BuildingSystem").GetComponent<BuildingSystem>();
        nodeRenderer = GetComponent<Renderer>();
        originColor = nodeRenderer.material.color;
    }

    public void ResetNode()
    {
        if (hasHero)
        {
            //Debug.Log("hi");
            hasHero = false;
        }
    }

    private void OnMouseEnter()
    {
        if (buildingSystem.GetHeroModel() != null && !hasHero)
        {
            nodeRenderer.material.color = Color.black;
        }
    }

    private void OnMouseExit()
    {
        nodeRenderer.material.color = originColor;
    }

    public void SetHasHero(bool hasHero)
    {
        this.hasHero = hasHero;
    }

    public bool GetHasHero()
    {
        return hasHero;
    }
}
