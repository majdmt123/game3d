using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingSystem : MonoBehaviour
{
    private HeroModel currentHero;
    private HeroModel currentHero1;
    public Shop shop;

    public GameObject summonHeroEffect;
    private void Start()
    {

    }

    public void BuyHero(HeroModel hero)
    {
        currentHero = hero;
        currentHero1 = hero;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            BuildHero();
        }
        
    }

    public void BuildHero()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Node"))
            {
                Vector3 blockSize = hit.collider.bounds.size;
                Vector3 blockPosition = hit.collider.transform.position;
                Vector3 towerPosition = blockPosition + new Vector3(0, blockSize.y, 0);
                if (!hit.collider.GetComponent<Node>().GetHasHero())
                {
                    shop.BuyHeroSuccess(currentHero);
                    Instantiate(currentHero.GetHeroPrefab(), towerPosition, Quaternion.Euler(0, 90, 0));
                    Instantiate(summonHeroEffect, towerPosition, Quaternion.Euler(-90, 0, 0));
                    
                    hit.collider.GetComponent<Node>().SetHasHero(true);

                    currentHero = null;
                }
            }
        }
    }

    public HeroModel GetHeroModel()
    {
        return currentHero;
    }
    
    public HeroModel GetHeroModel1()
    {
        return currentHero1;
    }
}
