using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

[Serializable]
public class MenuGame : MonoBehaviour
{
    public SceneFader sceneFader;
    public GameObject settingGUI;
    public GameObject menuGUI;
    public GameObject howToPlayGUI;
    public GameObject viewTrophyGUI;
    public TextMeshProUGUI levelMaxUnlockedGUI;

    private void Start()
    {
        int levelReached = PlayerPrefs.GetInt("levelReached", 1);
        levelMaxUnlockedGUI.text = "Level: " + levelReached.ToString();
    }
    public void PlayGame()
    {
        sceneFader.FadeTo("LevelScene");
        //SceneManager.LoadScene();
    }
    public void SettingOn()
    {
        menuGUI.SetActive(false);
        settingGUI.SetActive(true);
        
    }
    public void MenuOn()
    {
        settingGUI.SetActive(false);
        howToPlayGUI.SetActive(false);
        viewTrophyGUI.SetActive(false);
        menuGUI.SetActive(true);
    }

    public void HowToPlay()
    {
        howToPlayGUI.SetActive(true);
        menuGUI.SetActive(false);
    }
    public void ViewTroPhy()
    {
        viewTrophyGUI.SetActive(true);
        menuGUI.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
