using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class Shop : MonoBehaviour
{
    //Hero in shop by MVC
    public HeroModel[] heroModels;
    private HeroModel heroBuyed;

    public float money;

    public TextMeshProUGUI moneyToBuy;
    public BuildingSystem buildingSystem;

    public Image countdownBuyHero1;
    public Image countdownBuyHero2;
    public Image countdownBuyHero3;
    public Image countdownBuyHero4;
    public Image countdownBuyHero5;

    public Image canBuyHero1UI;
    public Image canBuyHero2UI;
    public Image canBuyHero3UI;
    public Image canBuyHero4UI;
    public Image canBuyHero5UI;

    public float countdown;
    //private bool canBuy = true;

    private bool canBuyHero1 = true;
    private bool canBuyHero2 = true;
    private bool canBuyHero3 = true;
    private bool canBuyHero4 = true;
    private bool canBuyHero5 = true;

    private void Start()
    {
        heroBuyed = new HeroModel();
        UpdateMoney();
    }

    private void Update()
    {
        

        ResetCountdown(countdownBuyHero1);
        ResetCountdown(countdownBuyHero2);
        ResetCountdown(countdownBuyHero3);
        ResetCountdown(countdownBuyHero4);
        ResetCountdown(countdownBuyHero5);

        CanBuyHero(0, canBuyHero1UI);
        CanBuyHero(1, canBuyHero2UI);
        CanBuyHero(2, canBuyHero3UI);
        CanBuyHero(3, canBuyHero4UI);
        CanBuyHero(4, canBuyHero5UI);

    }

    public void UpdateMoney()
    {
        moneyToBuy.text = money.ToString();
    }

    private void BuyHero()
    {
        buildingSystem.BuyHero(heroBuyed);
        heroBuyed = null;

    }

    public void BuyHeroSuccess(HeroModel heroModel)
    {
        money -= heroModel.GetCost();
        UpdateMoney();
    }
    
    public void KilledZombie(ZombieModel zombieModel)
    {
        money += zombieModel.GetCost();
        UpdateMoney();
    }

    public float GetMoney()
    {
        return money;
    }

    // Onclick by Player
    public void BuyHero1()
    {
        if (canBuyHero1 && canBuyHero1UI.fillAmount == 0)
        {
            heroBuyed = heroModels[0];
            BuyHero();
            StartCoroutine(ClickDelayCoroutine(countdownBuyHero1, value => canBuyHero1 = value));
        }
    }
    public void BuyHero2()
    {
        if (canBuyHero2 && canBuyHero2UI.fillAmount == 0)
        {
            heroBuyed = heroModels[1];
            BuyHero();
            StartCoroutine(ClickDelayCoroutine(countdownBuyHero2, value => canBuyHero2 = value));
        }
    }
    public void BuyHero3()
    {
        if (canBuyHero3 && canBuyHero3UI.fillAmount == 0)
        {
            heroBuyed = heroModels[2];
            BuyHero();
            StartCoroutine(ClickDelayCoroutine(countdownBuyHero3, value => canBuyHero3 = value));
        }
    }
    public void BuyHero4()
    {
        if (canBuyHero4 && canBuyHero4UI.fillAmount == 0)
        {
            heroBuyed = heroModels[3];
            BuyHero();
            StartCoroutine(ClickDelayCoroutine(countdownBuyHero4, value => canBuyHero4 = value));
        }
    }
    public void BuyHero5()
    {
        if (canBuyHero5 && canBuyHero5UI.fillAmount == 0)
        {
            heroBuyed = heroModels[4];
            BuyHero();
            countdownBuyHero5.fillAmount = 1;
            StartCoroutine(ClickDelayCoroutine(countdownBuyHero5, value => canBuyHero5 = value));
        }
    }

    public void CanBuyHero(int i, Image canBuyHeroUI)
    {
        if (heroModels[i].GetCost() > money)
        {
            canBuyHeroUI.fillAmount = 1;
        }
        else
        {
            canBuyHeroUI.fillAmount = 0;
        }
    }

    public void ResetCountdown(Image countdownBuyHero)
    {
        if (countdownBuyHero.fillAmount > 0)
        {
            countdownBuyHero.fillAmount -= Time.deltaTime * 1 / countdown;
        }
    }

    public IEnumerator ClickDelayCoroutine(Image countdownBuyHero, Action<bool> canBuy)
    {
        countdownBuyHero.fillAmount = 1;
        canBuy.Invoke(false);
        yield return new WaitForSeconds(countdown);
        canBuy.Invoke(true);
    }


}
