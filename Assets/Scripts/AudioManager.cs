using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    public AudioClip clickSound;
    public AudioClip shotSound;
    public AudioClip swordHitSound;
    public AudioClip zombieHitSound;
    public AudioClip dieSound;
    public AudioClip winSound;
    public AudioClip loseSound;
    public AudioClip laserSound;
    public AudioClip explosionSound;

    public TextMeshProUGUI soundState;

    private AudioSource audioSource;

    public static bool muteSound = false;

    public GameObject soundOnButton;
    public GameObject soundOffButton;

    public bool isPlay = true;
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.mute = muteSound;
        if (muteSound)
        {
            soundState.text = "OFF";
            soundOffButton.SetActive(true);
            soundOnButton.SetActive(false);
        }
        else
        {
            soundState.text = "ON";
            soundOnButton.SetActive(true);
            soundOffButton.SetActive(false);
        }
    }

    public void ToggleMute()
    {
        muteSound = !muteSound;
        audioSource.mute = muteSound;
        if (muteSound)
        {
            //soundState.text = "OFF";
            soundOffButton.SetActive(true);
            soundOnButton.SetActive(false);
        }
        else
        {
            //soundState.text = "ON";
            soundOnButton.SetActive(true);
            soundOffButton.SetActive(false);
        }
    }


    public void LoseSound()
    {
        audioSource.PlayOneShot(clickSound);
    }

    public void WinSound()
    {
        audioSource.PlayOneShot(winSound);
    }

    public void ShotSound()
    {
        audioSource.PlayOneShot(shotSound);
    }
    public void SwordHitSound()
    {
        audioSource.PlayOneShot(swordHitSound);
    }
    public void ZombieHitSound()
    {
        audioSource.PlayOneShot(zombieHitSound);
    }

    public void DieSound()
    {
        audioSource.PlayOneShot(dieSound);
    }

    public void ExplosionSound()
    {
        audioSource.PlayOneShot(explosionSound);
    }
    public void LaserSound()
    {
        if (isPlay)
        {
            audioSource.Play();
        }
    }
    public void OffLaserSound()
    {
        audioSource.Stop();
    }

}